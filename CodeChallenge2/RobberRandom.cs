﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge2
{
    public class RobberRandom : IRobberStrategy
    {
        public Point calculateNextMove(Point[] cops, Point robber)
        {
            return robber.randomMove();
        }   
    }
}
