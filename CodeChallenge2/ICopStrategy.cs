﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge2
{
    public interface ICopStrategy
    {
        Point[] calculateNextMove(Point[] cops, Point robber);
    }
}
