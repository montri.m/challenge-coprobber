﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeChallenge2
{
    public class Game
    {
        private readonly ICopStrategy copStrategy;
        private readonly IRobberStrategy robberStrategy;

        private Point[] cops;
        private Point robber;
        private int delay;

        public Game(ICopStrategy copStrategy, int delay = 100) : this(copStrategy, new RobberRandom(), delay) {
            
        }

        public Game(ICopStrategy copStrategy, IRobberStrategy robberStrategy, int delay = 100)
        {
            this.copStrategy = copStrategy;
            this.robberStrategy = robberStrategy;
            this.delay = delay;

            cops = new Point[] { new Point(0, 0), new Point(0, 0), new Point(0, 0) };
            robber = new Point(Point.MAX_POINT, Point.MAX_POINT);
        }

        public int run() 
        {
            for (int i = 0; i < 150; i++) 
            {
                if (processTurn(i)) 
                {
                    Console.WriteLine("Robber is caught at turn:" + i);
                    return i;
                }
            }
            Console.WriteLine("Turn Limit (150) exceeded.  Game end with a tie");
            return -1;
        }

        private void render(int turn, String note) {
            Console.Clear();
            Console.WriteLine("Turn #" + turn + " - " + note) ;
            renderBorder();
            for (int y = 0; y <= Point.MAX_POINT; y++) 
            {
                for (int x = 0; x <= Point.MAX_POINT; x++) 
                {
                    Point p = new Point(x, y);
                    Console.Write(getSign(p));
                }
                Console.WriteLine();
            }
            renderBorder();
            Thread.Sleep(delay);
        }

        private void renderBorder() {
            for (int i = 0; i <= Point.MAX_POINT; i++)
            {
                Console.Write("-");
            }
            Console.WriteLine();
        }

        private char getSign(Point p) 
        {
            if (isCop(p) && isRobber(p)) return '#';
            if (isCop(p)) return 'O';
            if (isRobber(p)) return 'X';
            return ' ';
        }

        private bool isCop(Point p) {
            return p.Equals(cops[0]) || p.Equals(cops[1]) || p.Equals(cops[2]);
        }

        private bool isRobber(Point p) {
            return p.Equals(robber);
        }

        private bool processTurn(int turn) 
        {
            Point[] nextTurnCops = copStrategy.calculateNextMove(cops, robber);
            Point nextTurnRobber = robberStrategy.calculateNextMove(cops, robber);

            if (nextTurnCops.Length != 3) {
                throw new Exception("Please return array with length 3 for cops.");
            }

            Point[] validNextCops = new Point[3];
            for (int i = 0; i < 3; i++)
            {
                validNextCops[i] = new Point(copMoveValid(cops[i], nextTurnCops[i])? nextTurnCops[i] : cops[i]);
            }
            
            Point validNextRobber = new Point(robberMoveValid(robber, nextTurnRobber)? nextTurnRobber : robber);
            robber = validNextRobber;
            render(turn, "Robber move");

            if (isRobberCaught(cops, robber)) 
            {
                return true;
            }

            cops = validNextCops;
            render(turn, "Cop move");

            if (isRobberCaught(cops, robber)) {
                return true;
            }

            return false;
        }
        
        private bool copMoveValid(Point current, Point newValue) 
        {
            if (newValue == null)
                throw new ArgumentNullException();

            if (!newValue.isValid())
                throw new ArgumentOutOfRangeException(newValue + " is out of bound");

            if (!newValue.isDirectAdjacent(current))
                throw new ArgumentOutOfRangeException(newValue + " is invalid move for cop");

            return true;
        }

        private bool robberMoveValid(Point current, Point newValue)
        {
            if (newValue == null)
                throw new ArgumentNullException();

            if (!newValue.isValid())
                throw new ArgumentOutOfRangeException(newValue + " is out of bound");

            if (!newValue.isAdjacent(current))
                throw new ArgumentOutOfRangeException(newValue + " is invalid move for robber");

            return true;
        }

        private bool isRobberCaught(Point[] cops, Point robber) 
        {
            for (int i = 0; i < 3; i++)
            {
                if (robber.Equals(cops[i]))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
