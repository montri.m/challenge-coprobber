﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeChallenge2
{
    public class Point
    {
        public const int MAX_POINT = 19;
        private static Random random = new Random();

        public int X { get; set; }
        public int Y { get; set; }

        public Point() { }

        public Point(int x, int y) {
            this.X = x;
            this.Y = y;
        }

        public Point(Point p)
        {
            this.X = p.X;
            this.Y = p.Y;
        }

        internal Point move(Direction direction) {
            int newX = X + getOffsetX(direction);
            int newY = Y + getOffsetY(direction);

            return new Point
            {
                X = Math.Min(Math.Max(0, newX), MAX_POINT),
                Y = Math.Min(Math.Max(0, newY), MAX_POINT),
            };
        }

        private int getOffsetX(Direction direction) {
            switch (direction) { 
                case Direction.TOP:
                case Direction.TOPLEFT:
                case Direction.TOPRIGHT:
                    return -1;
                case Direction.DOWN:
                case Direction.DOWNLEFT:
                case Direction.DOWNRIGHT:
                    return 1;
                default:
                    return 0;
            }
        }

        private int getOffsetY(Direction direction) {
            switch (direction) { 
                case Direction.LEFT:
                case Direction.TOPLEFT:
                case Direction.DOWNLEFT:
                    return -1;
                case Direction.RIGHT:
                case Direction.TOPRIGHT:
                case Direction.DOWNRIGHT:
                    return 1;
                default:
                    return 0;
            }
        }

        internal bool isAdjacent(Point p)
        {
            return (Math.Abs(X - p.X)) <= 1 && (Math.Abs(Y - p.Y) <= 1);
        }

        internal bool isDirectAdjacent(Point p)
        {
            return isAdjacent(p) && ((X == p.X) || (Y == p.Y));
        }

        internal bool isValid()
        {
            var validP = move(Direction.NONE);
            return this.Equals(validP);
        }

        internal Point randomMove()
        {
            return move((Direction)random.Next(9));
        }

        public override bool Equals(object obj)
        {
            Point o = obj as Point;
            if (o == null)
                return false;

            return (X == o.X && Y == o.Y);
        }

        public override int GetHashCode()
        {
            return X * 31 + Y;
        }

        public override string ToString()
        {
            return "[" + X + ", " + Y + "]";
        }
    }

    internal enum Direction { NONE, TOP, DOWN, LEFT, RIGHT, TOPLEFT, TOPRIGHT, DOWNLEFT, DOWNRIGHT };
}
