﻿using CodeChallenge2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CodeChallenge2
{
    public class SimpleCop : ICopStrategy
    {
        private static Random random = new Random();
        
        public Point[] calculateNextMove(Point[] cops, Point robber)
        {
            Point[] nextMove = new Point[cops.Length];

            for (int i = 0; i < cops.Length; i++)
            {
                int moveX = getMoveBit(robber.X, cops[i].X);
                int moveY = getMoveBit(robber.Y, cops[i].Y);

                nextMove[i] = randomMove(cops[i], moveX, moveY);
            }
            
            return nextMove;
        }

        private static int getMoveBit(int robber, int cop) {
            int offset = robber - cop;
            return (offset == 0) ? 0 : (offset / Math.Abs(offset));
        }

        private static Point randomMove(Point p, int moveX, int moveY)
        {
            int axis = random.Next(2);

            int actionX = (axis == 0) ? moveX : 0;
            int actionY = (axis == 0) ? 0 : moveY;

            return new Point
            {
                X = Math.Min(Math.Max(0, p.X + actionX), Point.MAX_POINT),
                Y = Math.Min(Math.Max(0, p.Y + actionY), Point.MAX_POINT),
            };
        }
    }
}
