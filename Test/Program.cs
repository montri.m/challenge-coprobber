﻿using CodeChallenge2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            var game = new Game(new SimpleCop());
            game.run();
            Console.ReadLine();
        }
    }
}
